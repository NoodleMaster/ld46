extends Item

enum LookDir {UP, DOWN, LEFT, RIGHT}

export(SpriteFrames) var upAnimation: SpriteFrames
export(SpriteFrames) var downAnimation: SpriteFrames
export(SpriteFrames) var leftAnimation: SpriteFrames
export(SpriteFrames) var rightAnimation: SpriteFrames

export(int) var speed: int = 16

var lookDir = LookDir.DOWN
var isMoving: bool = false
var animInvalid: bool = true
var prevDir: Vector2 = Vector2()

var isExtinguishing: bool = false

func _ready():
	self.add_to_group("Fireman")

func _process(delta: float):
	do_move(delta)
	$CPUParticles2D.emitting = isExtinguishing
	if(isExtinguishing):
		do_extinguish(delta)
	if(animInvalid):
		do_look()
		animInvalid = false


func do_extinguish(delta: float):
	var fire: Fire = self.fireRef
	if(fire == null):
		return
	fire.fuel -= delta * fire.fuelDecay * 2
	$AnimatedSprite.playing = false
	$AnimatedSprite.frame = 0
	$CPUParticles2D.direction = position.direction_to(fire.position).normalized()


func do_look():
	var animatedSprite: AnimatedSprite = $AnimatedSprite
	if(lookDir == LookDir.UP):
		animatedSprite.frames = upAnimation
	elif(lookDir == LookDir.DOWN):
		animatedSprite.frames = downAnimation
	elif(lookDir == LookDir.LEFT):
		animatedSprite.frames = leftAnimation
	elif(lookDir == LookDir.RIGHT):
		animatedSprite.frames = rightAnimation
	
	if(isMoving):
		animatedSprite.playing = true
		animatedSprite.frame = 1
	else:
		animatedSprite.playing = false
		animatedSprite.frame = 0


func do_move(delta: float):
	var fire: Fire = self.fireRef
	if(fire == null):
		return
	var dir := position.direction_to(fire.position).normalized()
	if(dir.y < -0.25):
		lookDir = LookDir.UP
	if(dir.y > 0.25):
		lookDir = LookDir.DOWN
	if(dir.x < -0.25):
		lookDir = LookDir.LEFT
	if(dir.x > 0.25):
		lookDir = LookDir.RIGHT
	animInvalid = prevDir.distance_to(dir) > 0.1
	prevDir = dir
	if(position.distance_to(fire.position) <= 50):
		isExtinguishing = true
		isMoving = false
	else:
		isMoving = true
		isExtinguishing = false
		move_and_collide(speed * delta * dir)
