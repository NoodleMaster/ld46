extends Node2D
class_name Fire

const nodeName: String = "Fire"

export (NodePath) var playerNodePath: NodePath
export(Array, float) var thresholds = []

export(Array, SpriteFrames) var thresholdSprites = []
export(Array, int) var thresholdOffsets = []

export(float) var fuelDecay: float = 1
export(float) var initialFuel: float = 10

var fuel: float = 0

onready var spritesRef: AnimatedSprite = $Sprites
var lastSpriteThresholdIndex: int = -1

var volumeOffset: float = 0

export var gameOver: bool = false

# Called when the node enters the scene tree for the first time.
func _ready():
	self.fuel = self.initialFuel

func updateSprite():
	
	#sorry man
	
	var threshAmount: int = thresholds.size()
	var bigT: int = 0
	
	for t in range(0, threshAmount):
		var thresh = thresholds[t]
		if(fuel >= thresh):
			bigT = t
	
	var interpol: float
	if(bigT == threshAmount - 1):
		interpol = (fuel - thresholds[bigT]) * 0.025
	else:
		interpol = (fuel - thresholds[bigT]) / (thresholds[bigT+1] - thresholds[bigT])
		
	$Light2D.energy = min(bigT + interpol + randf() * 0.1, 5)
	$Light2D.texture_scale = 0.5 + (bigT as float)/2 + interpol/2 + randf() * 0.1
	
	spritesRef.scale = Vector2(1, 1) * (1 + interpol)
	if(bigT != lastSpriteThresholdIndex):
		var sprites = self.thresholdSprites[bigT]
		self.spritesRef.frames = sprites
		self.spritesRef.offset = Vector2(0, self.thresholdOffsets[bigT])
		if(bigT == 0):
			volumeOffset = 0
			$FireAudioBig.playing = false
			$FireAudioSmall.playing = false
		elif(bigT == 1):
			volumeOffset = 0
			$FireAudioBig.playing = false
			$FireAudioSmall.playing = true
		elif(bigT == 2):
			volumeOffset = 0
			$FireAudioBig.playing = true
			$FireAudioSmall.playing = false
		elif(bigT == 3):
			volumeOffset = 10
			$FireAudioBig.playing = true
			$FireAudioSmall.playing = false
		lastSpriteThresholdIndex = bigT


func do_fuel(item: Item):
	if (self.gameOver):
		return
	
	if(item.fuel > 0):
		$FuelAudio.play()
	else:
		$BadFuelAudio.play()
	
	self.fuel += item.fuel
	self.updateSprite()
	item.queue_free()

func _process(delta):	
	var player = get_node(playerNodePath)
	if(player != null):
		$FireAudioSmall.volume_db = volumeOffset - 0.1 * player.position.distance_to(position)
		$FireAudioBig.volume_db = volumeOffset - 0.1 * player.position.distance_to(position)
	
	self.fuel -= self.fuelDecay * delta
	
	if (self.fuel < 0):
		self.fuel = 0
	
	self.updateSprite()
	
	if (self.fuel <= 0):
		self.gameOver = true

