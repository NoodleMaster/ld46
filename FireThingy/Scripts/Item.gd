extends KinematicBody2D
class_name Item

const GROUP_NAME = "Item"

export(int) var fuel: int = 0;
export(int) var insanity: int = 0

var fireRef
var playerRef

# Called when the node enters the scene tree for the first time.
func _ready():
	self.add_to_group(self.GROUP_NAME)
