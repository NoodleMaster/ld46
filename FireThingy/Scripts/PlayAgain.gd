extends Button

var scene = load("res://Scenes/Main.tscn")

func _on_PlayAgain_pressed():
	get_tree().change_scene_to(scene)


func _on_Start_pressed():
	get_tree().change_scene_to(scene)
