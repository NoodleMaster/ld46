extends Label

var score: int = 0
var counter: float = 0

func _process(delta: float):
	counter += delta
	if(counter >= 1):
		counter = 0
		score += 1
		update_text()
	Global.score = score


func update_text():
	text = "Score: " + String(score)
