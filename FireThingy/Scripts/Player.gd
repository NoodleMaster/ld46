extends KinematicBody2D
class_name Player

enum LookDir {UP, DOWN, LEFT, RIGHT}

export (NodePath) var fireNodePath: NodePath

export(SpriteFrames) var upAnimationH: SpriteFrames
export(SpriteFrames) var downAnimationH: SpriteFrames
export(SpriteFrames) var leftAnimationH: SpriteFrames
export(SpriteFrames) var rightAnimationH: SpriteFrames

export(SpriteFrames) var upAnimationM: SpriteFrames
export(SpriteFrames) var downAnimationM: SpriteFrames
export(SpriteFrames) var leftAnimationM: SpriteFrames
export(SpriteFrames) var rightAnimationM: SpriteFrames

export(SpriteFrames) var upAnimationI: SpriteFrames
export(SpriteFrames) var downAnimationI: SpriteFrames
export(SpriteFrames) var leftAnimationI: SpriteFrames
export(SpriteFrames) var rightAnimationI: SpriteFrames

export(int) var speed: int = 16
export(int) var interactDistance: int = 100
export(float) var itemHoldDistance: float = 32
export(int) var dropDistance: int = 100

export(int) var midInsanityLevel = 10
export(int) var highInsanityLevel = 20
export(int) var endInsanityLevel = 50
export(float) var stepFreq = 2

export(int) var initialInsanity = 0

var audioCounter: float = 0
var lookDir = LookDir.DOWN
var isMoving: bool = false
var animInvalid: bool = false
var prevDir: Vector2 = Vector2()
var insanity: int = 0
var heldItem: Item = null

func _ready():
	$Light2D.look_at(position + Vector2(0, 1) * 200)
	$Light2D.rotate(PI * 0.5)
	$Light2D.position = 32*Vector2(0, 1)
	self.insanity = self.initialInsanity

func _process(delta: float):
	audioCounter += delta
	do_move(delta)
	do_interact()
	do_sound()
	if(animInvalid):
		do_look()
		animInvalid = false
	if(heldItem != null):
		heldItem.position = position + Vector2(0, -itemHoldDistance)


func do_sound():
	if(isMoving && audioCounter > 1/stepFreq):
		$StepPlayer.play()
		audioCounter = 0


func do_look():
	var animatedSprite: AnimatedSprite = $AnimatedSprite
	if(insanity >= highInsanityLevel):
		if(lookDir == LookDir.UP):
			animatedSprite.frames = upAnimationI
		elif(lookDir == LookDir.DOWN):
			animatedSprite.frames = downAnimationI
		elif(lookDir == LookDir.LEFT):
			animatedSprite.frames = leftAnimationI
		elif(lookDir == LookDir.RIGHT):
			animatedSprite.frames = rightAnimationI
	elif(insanity >= midInsanityLevel):
		if(lookDir == LookDir.UP):
			animatedSprite.frames = upAnimationM
		elif(lookDir == LookDir.DOWN):
			animatedSprite.frames = downAnimationM
		elif(lookDir == LookDir.LEFT):
			animatedSprite.frames = leftAnimationM
		elif(lookDir == LookDir.RIGHT):
			animatedSprite.frames = rightAnimationM
	else:
		if(lookDir == LookDir.UP):
			animatedSprite.frames = upAnimationH
		elif(lookDir == LookDir.DOWN):
			animatedSprite.frames = downAnimationH
		elif(lookDir == LookDir.LEFT):
			animatedSprite.frames = leftAnimationH
		elif(lookDir == LookDir.RIGHT):
			animatedSprite.frames = rightAnimationH
	
	if(isMoving):
		animatedSprite.playing = true
		animatedSprite.frame = 1
	else:
		animatedSprite.playing = false
		animatedSprite.frame = 0


func do_interact():
	if(Input.is_action_just_released("interact")):
		var fire: Fire = get_node(fireNodePath)
		if(heldItem == null):
			var closestItem: Item = null
			var closestDist:float = interactDistance
			for item in get_tree().get_nodes_in_group(Item.GROUP_NAME):
				var dist: float = item.position.distance_to(position)
				if(dist <= closestDist):
					closestDist = dist
					closestItem = item
			if(closestItem != null):
				heldItem = closestItem
				$PickupPlayer.play()
		elif(fire != null && fire.position.distance_to(position) <= interactDistance):
			insanity += heldItem.insanity
			animInvalid = true
			fire.do_fuel(heldItem)
			heldItem = null
		else:
			drop_item()
			$DropPlayer.play()


func do_move(delta: float):
	var dir := Vector2()
	if(Input.is_action_pressed("up")):
		lookDir = LookDir.UP
		dir.y -= 1
	if(Input.is_action_pressed("down")):
		lookDir = LookDir.DOWN
		dir.y += 1
	if(Input.is_action_pressed("left")):
		lookDir = LookDir.LEFT
		dir.x -= 1
	if(Input.is_action_pressed("right")):
		lookDir = LookDir.RIGHT
		dir.x += 1
	dir = dir.normalized()
	if(dir.length() > 0.1):
		isMoving = true
		$Light2D.look_at(position + dir * 200)
		$Light2D.rotate(PI * 0.5)
		$Light2D.position = 32*dir
	else:
		isMoving = false
	animInvalid = prevDir.distance_to(dir) > 0.1
	prevDir = dir
	move_and_collide(speed * delta * prevDir)


func drop_item():
	if(heldItem == null):
		return
	var offsetPos := Vector2(0, 0)
	if(lookDir == LookDir.UP):
		offsetPos.y = -1
	elif(lookDir == LookDir.DOWN):
		offsetPos.y = 1
	elif(lookDir == LookDir.LEFT):
		offsetPos.x = -1
	elif(lookDir == LookDir.RIGHT):
		offsetPos.x = 1
	heldItem.position = position + (offsetPos * dropDistance)
	heldItem = null

