extends Node

class_name GameOverController

export (NodePath) var fireNodePath
var fireRef

export (NodePath) var playerNodePath
var playerRef

export(int) var insanityEndingThreshold = 100

export (PackedScene) var outOfFireEndingScene

func _ready():
	self.fireRef = get_node(self.fireNodePath)
	self.playerRef = get_node(self.playerNodePath)

func _process(_delta):
	if (self.fireRef.fuel <= 0):
		self.outOfFireEnding()

func outOfFireEnding():
	self.changeScene(self.outOfFireEndingScene)

func changeScene(scene: PackedScene):
	self.get_tree().change_scene_to(scene)
