extends Node2D

export(float) var riseSpeed: float = 32

export (NodePath) var fireNodePath: NodePath
export (NodePath) var playerNodePath: NodePath
var fireSpiritEnd = preload("res://Scenes/GameOver/InsaneEnding.tscn")

var latch: bool = false

var counter: float = 5

func _ready():
	visible = false


func _process(delta: float):
	if(counter <= 0):
		get_tree().change_scene_to(fireSpiritEnd)
	
	var fire: Fire = get_node(fireNodePath)
	var player: Player = get_node(playerNodePath)
	if(latch
	|| (player != null && fire != null
	&& player.position.distance_to(fire.position) < 128
	&& player.insanity >= player.endInsanityLevel
	&& fire.fuel >= fire.thresholds[-1])):
		fire.fuel = fire.thresholds[-1] + 1
		latch = true
		visible = true
		position.y -= delta * riseSpeed
		scale += delta * riseSpeed * Vector2(0.01, 0.01)
		counter -= delta
