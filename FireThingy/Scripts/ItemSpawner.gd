extends Node

export(Vector2) var firePostion
export(float) var fireNonSpawnRadius: float = 5

export(Vector2) var mapSize

export(float) var spawnInterval: float = 5
export(Array, PackedScene) var items = []

var lastSpawnDelta = 0
var rng = RandomNumberGenerator.new()

export (NodePath) var fireNodePath: NodePath
var fireRef: Fire

export(PackedScene) var waterSpirit
export(int) var waterSpiritMaxAmount = 0
export(float) var waterSpritiSpawnChance = 0.05

export(float) var timeAfterWaterSpiritMaxIncrease = 10
var timeElapsed: float = 0

export(PackedScene) var fireman
export(int) var firemanMaxAmount = 1

export (NodePath) var playerNodePath: NodePath
var playerRef: Player

export(int) var borderSize = 10
export(int) var tileSize = 32

# Called when the node enters the scene tree for the first time.
func _ready():
	self.rng.randomize()
	self.fireRef = get_node(self.fireNodePath)
	self.playerRef = get_node(self.playerNodePath)

func spawn(amount: int):
	for _i in range(0, amount):
		var waterSpiritAmount = get_tree().get_nodes_in_group(WaterSpirit.GROUP_NAME_WATER).size()
		var firemanAmount = get_tree().get_nodes_in_group("Fireman").size()
		var spawnChance = self.rng.randf_range(0, 1)
		
		var item: PackedScene
		if(spawnChance <= self.waterSpritiSpawnChance && waterSpiritAmount < self.waterSpiritMaxAmount):
			item = self.waterSpirit
		elif(self.playerRef.insanity >= self.playerRef.midInsanityLevel && firemanAmount < self.firemanMaxAmount):
			item = self.fireman
		else:
			var itemIndex = self.rng.randi_range(0, self.items.size() - 1)
			item = self.items[itemIndex]
		
		var instance: Item = item.instance()
		instance.fireRef = self.fireRef
		instance.playerRef = self.playerRef
		
		var pos = null
		while (
			pos == null || 
			(
				(
					pos.x > firePostion.x - fireNonSpawnRadius && 
					pos.x < firePostion.x + fireNonSpawnRadius && 
					pos.x > self.borderSize * self.tileSize && 
					pos.x < self.mapSize.x - (self.borderSize * self.tileSize)
				) && 
				(
					pos.y > firePostion.y - fireNonSpawnRadius && 
					pos.y < firePostion.y + fireNonSpawnRadius && 
					pos.y > self.borderSize * self.tileSize && 
					pos.y < self.mapSize.y - (self.borderSize * self.tileSize)
				)
			)
		):
			pos = Vector2(self.rng.randf_range(self.borderSize * self.tileSize, self.mapSize.x - (self.borderSize * self.tileSize)), self.rng.randf_range(self.borderSize * self.tileSize, self.mapSize.y - self.borderSize * self.tileSize))
		
		instance.position = pos
		self.add_child(instance)

func _process(delta):
	if (self.fireRef.gameOver):
		return
	
	self.lastSpawnDelta += delta
	self.timeElapsed += delta
	
	if (self.timeElapsed >= self.timeAfterWaterSpiritMaxIncrease):
		self.waterSpiritMaxAmount += 1
		self.timeElapsed = 0
	
	if (lastSpawnDelta >= self.spawnInterval):
		self.lastSpawnDelta -= self.spawnInterval
		self.spawn(1)
