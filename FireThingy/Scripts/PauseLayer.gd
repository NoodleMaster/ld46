extends Control


func _ready():
	visible = false

func _on_Quit_pressed():
	get_tree().quit(0)

func _on_Resume_pressed():
	get_tree().paused = false
	visible = false

func _input(event):
	if(event.is_action_pressed("pause")):
		get_tree().paused = !get_tree().paused
		visible = get_tree().paused
