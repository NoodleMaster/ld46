extends Item

export(float) var speed = 10
export(float) var distance = 250

export(NodePath) var spriteNodePath
var spriteRef: AnimatedSprite

var rng = RandomNumberGenerator.new()

export(Vector2) var mapSize
export(int) var borderSize = 10
export(int) var tileSize = 32

var moveTo = null

# Called when the node enters the scene tree for the first time.
func _ready():
	self.rng.randomize()
	self.spriteRef = get_node(self.spriteNodePath)

func _process(delta):
	if(self.moveTo != null && self.position.distance_to(self.moveTo) < 0.25):
		self.moveTo = null
	
	if(self.moveTo == null):
		
		while(
			moveTo == null ||
			moveTo.x < (self.borderSize * self.tileSize) ||
			moveTo.x > self.mapSize.x - (self.borderSize * self.tileSize) ||
			moveTo.y < (self.borderSize * self.tileSize) ||
			moveTo.y > self.mapSize.y - (self.borderSize * self.tileSize)
		):
			var posOffX = self.rng.randf_range(-(self.distance / 2), self.distance / 2)
			var posOffY = self.rng.randf_range(-(self.distance / 2), self.distance / 2)
		
			moveTo = self.position + Vector2(posOffX, posOffY)
			
	var dir = self.position.direction_to(self.moveTo)
	move_and_collide(self.speed * delta * dir.normalized())
	if(!weakref(spriteRef).get_ref()):
		return
	if(dir.x > 0):
		self.spriteRef.frame = 1
	else:
		self.spriteRef.frame = 0
