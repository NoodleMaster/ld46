extends Node

export(Array, StreamTexture) var textures = []
export(Vector2) var textureSize
export(Vector2) var mapSize

export(int) var borderSize = 10
export(PackedScene) var border

export(PackedScene) var tree
export(int) var treeAmount = 20

var rng = RandomNumberGenerator.new()

func _ready():
	self.rng.randomize()
	
	var width = ceil(self.mapSize.x / self.textureSize.x)
	var height = ceil(self.mapSize.y / self.textureSize.y)
	
	for x in range (0, width):
		for y in range (0, height):
			if(x < self.borderSize || y < self.borderSize || x > (width - self.borderSize) || y > (height - self.borderSize)):
				var instance = self.border.instance()
				instance.position = Vector2(x * self.textureSize.x, y * self.textureSize.y)
				self.add_child(instance)
				continue
			
			var textureIndex = self.rng.randi_range(0, textures.size() - 1)
			var texture = self.textures[textureIndex]
			
			var sprite = Sprite.new()
			sprite.position = Vector2(x * self.textureSize.x, y * self.textureSize.y)
			sprite.centered = false
			sprite.texture = texture
			
			self.add_child(sprite)

	for _i in range (0, self.treeAmount):
		var x = self.rng.randi_range((self.borderSize + 1) * self.textureSize.x, (width - self.borderSize - 1) * self.textureSize.x)
		var y = self.rng.randi_range((self.borderSize + 1) * self.textureSize.y, (height - self.borderSize - 1) * self.textureSize.y)
		if(x > 700 && x < 800
		&& y > 700 && y < 800):
			continue
		var instance = self.tree.instance()
		instance.position = Vector2(x , y)
		self.add_child(instance)
		
	
