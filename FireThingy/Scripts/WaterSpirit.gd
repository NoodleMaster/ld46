extends Item
class_name WaterSpirit

const GROUP_NAME_WATER = "WaterSpririt"

export(float) var interactionRadius
export(float) var speed

func _ready():
	self.add_to_group(GROUP_NAME_WATER)
	
func _process(delta):
	var dir = self.position.direction_to(self.fireRef.position)
	move_and_collide(self.speed * delta * dir)
	
	if (self.position.distance_to(self.fireRef.position) <= self.interactionRadius && self.playerRef.heldItem != self):
		self.fireRef.do_fuel(self)
